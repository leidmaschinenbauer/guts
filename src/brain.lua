local brain = { valence = 0, feeling = 0, target = 0, expression = "neutral" }

local t = 0
function brain:tick(dt, button)
  -- positivity prior and mood swings
  t = t + dt
  target = self.target + (math.sin(t*2*math.pi/60/60/12) * .15)

  local d = target - self.feeling
  if button.pressed then
    d = -3
  elseif d < 0 then
    d = math.max(-1, d)
  elseif d > 0 then
    d = math.min(1, d)
  end
  self.feeling = math.max(-3, math.min(3, self.feeling + d * dt / 10))
  self.valence = math.tanh(self.feeling*5)

  -- change expression depending on emotional state
  self.expression = 'neutral'
  if self.valence < -.5 then
    self.expression = "terror"
  elseif self.valence < -.2 then
    self.expression = "loathing"
  elseif self.valence < -.1 then
    self.expression = "vigilance"
  elseif self.valence > .4 then
    self.expression = "ecstasy"
  elseif self.valence > .2 then
    self.expression = "admiration"
  end
end

function brain:see(people)
  self.target = 0
  if #people == 0 then return end

  -- RSME of people valences
  local v = 0
  for _, p in ipairs(people) do
    v = v + ((1 + p.valence)/2)^2
  end
  v = math.sqrt(v / #people)

  self.target = math.log(v/(1-v)) -- map to -inf to +inf
  if self.target >= 0 then
    self.target = self.target + .1 -- positive people prior
  end
end

return brain
