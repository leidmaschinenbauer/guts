local game = {}
local Shape = require "shape"
local Color = require "color"

local WIDTH, HEIGHT = love.graphics.getWidth(), love.graphics.getHeight()

local padding = 4
local cemocolor = {0,0,0}
local camera = libs.picocam.new{
  width = WIDTH/2,
  height = HEIGHT,
}
local camerarot = 0
local segments = {}

local emotion_types = {"neutral", "admiration", "ecstasy", "loathing", "rage",
                       "vigilance", "amazement", "grief", "terror", "pain"}

local brain = require "brain"

local button = {pressed = false, t = 0}
local people = {}

local fonts = {
  code = love.graphics.newFont("assets/UbuntuMono-R.ttf",24)
}

love.graphics.setFont(fonts.code)
local code = {
  text = love.graphics.newText(fonts.code, love.filesystem.read("code"):gsub("[^%g \n]", "$")),
  offset = 0
}
code.scrollheight = code.text:getHeight() - 2*HEIGHT


local sound = {}
for _,k in ipairs{"acid", "pulses", "voltage", "telephone", "ping"} do
  sound[k] = love.audio.newSource(("assets/%s.wav"):format(k), 'static')
end

sound.scream = love.audio.newSource({"assets/scream1.wav", "assets/scream2.wav",
                                     "assets/scream3.wav", "assets/scream4.wav"},
                                    'static')

for _,k in ipairs{"acid", "pulses", "voltage"} do
  sound[k]:setLooping(true)
  sound[k]:addTags("thebutton")
end

local noise = love.image.newImageData(WIDTH, HEIGHT)
noise:mapPixel(function(x,y)
  local v = (1-love.math.noise(x/WIDTH*7,y/HEIGHT*29))^.5 * 255
  return v,v,v,255
end)
noise = love.graphics.newImage(noise)

function game:enter(args)
  sound.telephone:play()
  sound.telephone:setVolume(.002)
  -- setup emotions
  for i,v in pairs(emotion_types) do
    local raw = love.filesystem.read("assets/emotions/"..v..".json")
    local data = libs.json.decode(raw)
    segments[v] = Shape(data):scale(8,8,6):translate(0,0,-3)
  end

  segments.current = segments.neutral:clone()
  segments.tween_to = segments.neutral

  -- setup effects
  post_effect = (require 'shader')()
  post_effect.grain_size = 1.3
  post_effect.grain_opacity = 0.5
  post_effect.vignette_radius = 0.9
  post_effect.vignette_opacity = 0.9

  -- overall timer
  libs.timer.every(.1, function() self:getEvent() end)
  libs.timer.every(.25, function() self:sendValenceEvent() end)

  -- idle animation
  self.timer_idle = libs.timer()
  self.timer_idle:script(function(wait)
    while true do
      local theta = love.math.random()-.5 + math.pi -- ∈ [π-.5:π+.5]
      self.timer_idle:tween(.3+love.math.random()*.2, camera, {theta=theta}, "out-back")
      wait(love.math.random() * 2 + .5)
    end
  end)
  self.timer_idle:script(function(wait)
    while true do
      local o = (2*love.math.random() - 1) * code.scrollheight
      self.timer_idle:tween(.2, code, {offset = o})
      wait(.2)

      local t = 5 + love.math.random() * 5
      local d = (2*love.math.random(2)-3) * (love.math.random(200) + 200)
      -- d ∈ [-400:400]
      self.timer_idle:tween(t, code, {offset = code.offset+d})
      wait(t)
    end
  end)
  self.timer_idle:script(function(wait)
    while true do
      sound.ping:setPitch(.8 + math.random()*.2)
      sound.ping:play()
      wait(2 + math.random() * 60)
    end
  end)

  -- has visitors timer
  self.timer_visitors = libs.timer()
  self.timer_visitors:script(function(wait)
    wait(0) -- hackety hack
    while true do
      local p = people[math.random(#people)]
      local theta = p and (-.5*p.center[1] + math.pi) or (love.math.random()-.5 + math.pi)

      -- note: valence can be negative
      local t = .2 + math.min(0,brain.valence)*.15
      self.timer_visitors:tween(t, camera, {theta=theta}, "out-back")
      wait(t)
    end
  end)
  self.timer_visitors:script(function(wait)
    while true do
      local o = (2*love.math.random() - 1) * code.scrollheight
      self.timer_visitors:tween(.2, code, {offset = o}, 'in-out-elastic')
      wait(love.math.random()*.2 + .2)
    end
  end)

  -- button held timer: shock sounds, screams
  self.timer_button = libs.timer()

  sound.telephone:setLooping(true)
  sound.telephone:play()
end

function game:draw()
  post_effect:draw(self.del_draw)

  if debug_mode then
    love.graphics.print(
      "FPS: "..love.timer.getFPS().."\n"..
      "#people: "..#people.."\n"..
      "valence: "..brain.valence.."\n"..
      "expression: "..brain.expression.."\n"..
      "", 32,32)
  end
end

local colors = {
  negative = Color.fromHSL(0,1,.5),
  neutral  = Color.fromHSL(.6,.3,.7),
  positive = Color.fromHSL(.336,.5,.6),

  eyes = {
    negative = Color.fromHSL(.8,.6,.8,.4),
    neutral  = Color.fromHSL(0,1,.5,.1),
    positive = Color.fromHSL(.6,.5,.6,.2),
  }
}
function game:del_draw()
  -- draw text
  local s = math.abs(brain.valence) * .5 -- ∈ [0,.5] => sat...desat...sat
  local h = .155 + .155 * brain.valence -- ∈ [0,.33] => red...orange...yellow...green
  local c = colors.neutral * (1 - brain.valence^2)
              + colors.negative * math.min(0, brain.valence)^2
              + colors.positive * math.max(0, brain.valence)^2
  if button.pressed then
    c = c * .6 + colors.negative * .4
  end
  love.graphics.setColor(c())

  love.graphics.draw(code.text, 0, code.offset, 0, 1, -1)
  love.graphics.draw(code.text, 0, code.offset, 0, 1, 1)

  -- simplex noise over code
  love.graphics.setColor(255,255,255)
  love.graphics.setBlendMode("multiply")
  love.graphics.draw(noise, 0, 0)
  love.graphics.setBlendMode("alpha")

  -- draw face
  love.graphics.push()
  love.graphics.translate(WIDTH/4, 0)

  -- random jitter when in fear
  segments.current:draw(camera, math.min(0,brain.valence)^2 * .02)

  -- draw eyes
  local left, right = segments.current:eyes(camera)
  local c = colors.eyes.neutral * (1 - brain.valence^2)
              + colors.eyes.negative * math.min(0, brain.valence)^2
              + colors.eyes.positive * math.max(0, brain.valence)^2
  love.graphics.setColor(c())
  love.graphics.polygon('fill', left)
  love.graphics.polygon('fill', right)
  love.graphics.pop()
end

function game:tween(cval,tval,speed)
  for i,c in ipairs(cval) do
    local d = c - tval[i]
    if d < -speed then -- cval[i] < tval[i] and |c - t| > speed
      cval[i] = c + speed
    elseif d > speed then -- c > t and |c - t| > speed
      cval[i] = c - speed
    else -- |c - t| <= speed
      cval[i] = tval[i]
    end
  end
end

function game:getRandomEvent()
  local events = love.filesystem.getDirectoryItems("events")
  local event = events[math.random(#events)]
  local raw = love.filesystem.read("events/"..event)
  return raw
end

function game:getInputEvent()
  for i,v in pairs(love.filesystem.getDirectoryItems("events/")) do
    if v:sub(1,3) == "det" then
      local fname = "events/"..v
      local raw = love.filesystem.read(fname)
      love.filesystem.remove(fname)
      return raw
    end
  end
end

function game:getEvent()
  local raw_json = self:getInputEvent()
  if raw_json then
    local ok, err = pcall(function()
      local event = libs.json.decode(raw_json)
      if event.type == "detection" then
        people = event.detections
        brain:see(people)
      end
      return true
    end)
    if not ok then
      print("json:", raw_json)
      print(err)
    end
  end
end

function game:sendButtonEvent()
  local uuids = {}
  for _,p in ipairs(people) do
    uuids[#uuids+1] = p.uuid
  end
  love.filesystem.write(("events/btn-%s"):format(love.timer.getTime()), libs.json.encode{
    t = button.t, uuids = uuids
  })
end

function game:sendValenceEvent()
  if #people == 0 then return end

  local uuids = {}
  for _,p in ipairs(people) do
    uuids[#uuids+1] = p.uuid
  end
  love.filesystem.write(("events/val-%s"):format(love.timer.getTime()), libs.json.encode{
    valence = brain.valence, uuids = uuids
  })
end

local t = 0
function game:update(dt)
  if love.keyboard.isDown("f") then
    dt = dt * 10
  end
  libs.timer.update(dt)
  self.timer_button:update(dt)

  t = (t+dt)%60
  post_effect.t = t
  brain:tick(dt, button)

  -- tweak shader if button pressed
  if button.pressed then
    button.t = (button.t or 0) + dt
    post_effect.exposure = .3
    post_effect.magnitude = {button.t/15, button.t/15}
    post_effect.melt = math.max(0,button.t-.5) / 300
  else
    post_effect.exposure = 0.25
  end

  -- update timers
  if #people > 0 or button.pressed then
    self.timer_visitors:update(dt)
  else
    self.timer_idle:update(dt)
  end

  -- live emotions
  if button.pressed then
    segments.tween_to = segments.pain
  else
    segments.tween_to = segments[brain.expression]
  end

  sound.ping:setVolume(math.sqrt(math.max(0, brain.valence)))
  sound.telephone:setVolume(math.max(0, -brain.valence)*.02)

  -- tween face
  for i,v in pairs(segments.current) do
    self:tween(segments.current[i][1], segments.tween_to[i][1], dt/5)
    self:tween(segments.current[i][2], segments.tween_to[i][2], dt/5)
  end
end

function game:keypressed(key)
--  if key == "s" then
--    local screenshot = love.graphics.newScreenshot();
--    screenshot:encode('png', os.time() .. '.png');
--  elseif key == "`" or key == "d" then
--    debug_mode = not debug_mode
--  elseif key == "e" then -- force event
--    self:getEvent()
--  elseif key == "space" or key == "w" or key == "o" then
    button.pressed = true
    button.t = 0

    -- audio cues for pain
    love.audio.tags.thebutton.play()
    love.audio.tags.thebutton.setVolume(0)
    sound.voltage:setVolume(.7)
    self.timer_button:script(function(wait)
      while true do
        sound.voltage:setPitch(.9 + love.math.random()*.2)
        wait(.05+love.math.random()*.3)
      end
    end)
    self.timer_button:tween(6, sound.acid,
      function(snd,s) snd:setVolume(s*.7) end, 'quad')
    self.timer_button:tween(3, sound.scream,
      function(snd,s) snd:setVolume(s) end)

    --[[ screams
    self.timer_button:script(function(wait)
      local s, T
      while true do
        s = sound.scream:play()
        s:seek(love.math.random()*(s:getDuration() - .2))
        wait(.01+love.math.random()*.09)
        s:stop()
        wait(.05+love.math.random()*.15)
      end
    end)--]]
--  end
end

function game:keyreleased(key)
--  if key == "space" or key == "w" or key == "o" then
    button.pressed = false
    local t = math.min(button.t, 15)
    libs.timer.tween(math.max(t/10,1), post_effect, function(effect, s)
      post_effect.magnitude = {t*(1-s), t*(1-s)}
      post_effect.melt = (t/200)*(1-s)
    end, "out-elastic")
    self:sendButtonEvent()
    button.t = 0

    love.audio.tags.thebutton.stop()
    sound.scream:stop()
    self.timer_button:clear()
--  end
end

return game
