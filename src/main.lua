-- Thanks @bartbes! fixes cygwin buffer
io.stdout:setvbuf("no")

math.randomseed(os.time())

libs = {
  picocam = require"libs.picocam",
  gamestate = require"libs.gamestate",
  timer = require"libs.timer",
  json = require"libs.json",
}
require"libs.slam"

states = {
  game = require"states.game",
  editor = require"states.editor",
}

function love.load(args)
  libs.gamestate.registerEvents()

  local target = states.game

  for i,v in pairs(args) do
    if states[v] then
      target = states[v]
    end
  end

  love.mouse.setVisible(false)
  libs.gamestate.switch(target)
end
