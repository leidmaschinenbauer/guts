local Color = {}
Color.__index__ = Color

Color.get = function(a)
  return a[1]*255,a[2]*255,a[3]*255,a[4]*255
end
Color.__call = Color.get

Color.__add = function(a,b)
  return Color(a[1]+b[1], a[2]+b[2], a[3]+b[3], a[4]+b[4])
end

Color.__mul = function(a,f)
  if type(f) ~= "number" then a,f = f,a end
  return Color(a[1]*f, a[2]*f, a[3]*f, a[4]*f)
end

Color.new = function(r,g,b,a)
  return setmetatable({r,g,b,a or 1}, Color)
end
setmetatable(Color, {__call = function(_,...) return Color.new(...) end})

Color.fromHSL = function(h, s, l, a)
  if s <= 0 then return l,l,l,a end

  h = h * 6

  local c = s * (1 - math.abs(2*l - 1))
  local x = c * (1 - math.abs(h%2 - 1))

  local r,g,b = c,0,x
  if h <= 1     then r,g,b = c,x,0
  elseif h <= 2 then r,g,b = x,c,0
  elseif h <= 3 then r,g,b = 0,c,x
  elseif h <= 4 then r,g,b = 0,x,c
  elseif h <= 5 then r,g,b = x,0,c
  end

  local m = l - c / 2
  return Color(r+m, g+m, b+m,a)
end

return Color
