game_name = "Leidmaschine"
git_hash,git_count = "missing git.lua",-1
pcall( function() return require("git") end );

function love.conf(t)
  t.window.width = 1920
  t.window.height = 1080
  t.window.title = game_name .. " [v"..git_count.."-"..git_hash.."]"
  t.identity = game_name
end
