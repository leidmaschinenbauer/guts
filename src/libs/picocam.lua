picocam = {
  new = function(init)
    init = init or {}
    local self = {}
    self.z = init.z or -3
    self.focallength = init.focallength or 5
    self.fov = init.fov or 45
    self.theta = init.theta or 0
    self.width = init.width or 128
    self.height = init.height or 256
    -- public
    self.transform = picocam.transform
    self.line = picocam.line
    self.point = picocam.point
    -- private
    self._perspective = picocam._perspective
    self._coordstopx = picocam._coordstopx
    self._map = picocam._map
    return self
  end,
  transform = function(self, px,py,pz)
    return self:_coordstopx(self:_perspective(px, py, pz))
  end,
  line = function(self, p1x,p1y,p1z, p2x,p2y,p2z)
    p1x,p1y = self:transform(p1x, p1y, p1z)
    love.graphics.line(p1x, p1y, self:transform(p2x, p2y, p2z))
  end,
  _perspective = function(self, x,y,z)
    local x_rot = x * math.cos(self.theta) - z * math.sin(self.theta)
    local z_rot = x * math.sin(self.theta) + z * math.cos(self.theta)
    local dz = z_rot - self.z
    local out_z = self.z + self.focallength
    local m_xz = x_rot / dz
    local m_yz = y / dz
    local out_x = m_xz * out_z
    local out_y = m_yz * out_z
    return out_x, out_y
  end,
  _map = function(v, a, b, c, d)
    local partial = (v - a) / (b - a)
    return partial * (d - c) + c
  end,
  _coordstopx = function(self,x,y)
    local radius = self.focallength * math.tan(self.fov / 2 )
    local pixel_x = self._map(x, -radius, radius, 0, self.width)
    local pixel_y = self._map(y, -radius, radius, 0, self.height)
    return pixel_x, pixel_y
  end
}

return picocam
