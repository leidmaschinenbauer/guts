return function()
  -- Double buffering
  local back = love.graphics.newCanvas()
  local front = love.graphics.newCanvas()
  local buffer = function()
    back, front = front, back
    return front
  end

  -- Godsray shader
  local godsray = love.graphics.newShader[[
    extern number exposure = 0.5;
    extern number decay = 0.95;
    extern number density = 0.05;
    extern number weight = 0.5;
    extern vec2 light_position = vec2(0.5,0.5);
    extern number samples = 70.0;

    vec4 effect(vec4 color, Image tex, vec2 uv, vec2 px) {
      vec2 offset = (uv - light_position) * density / samples;
      number illumination = 1.0;
      vec4 c = vec4(.0, .0, .0, 1.0);

      for (int i = 0; i < samples; ++i) {
        uv -= offset;
        c += Texel(tex, uv) * illumination * weight;
        illumination *= decay;
      }

      return vec4(c.xyz * exposure, 1.0);
    }
  ]]


  -- noise texture for below
  local noise = love.image.newImageData(100,100)
  noise:mapPixel(function()
    local l = love.math.random() * 255
    return l,l,l,l
  end)
  noise = love.graphics.newImage(noise)

  -- Degredation shader: barrel distortion, filmgrain, vignette
  local degrade = love.graphics.newShader[[
    // barrel distortion
    extern vec2 magnitude = vec2(0.0, 0.0);

    extern number melt = 0.0;

    // filmgrain
    extern number grain_opacity = 0.5;
    extern number grain_size = 1.0;
    extern number noise = 0.0;
    extern Image noisetex;
    extern vec2 tex_ratio;

    // vignette
    extern number vignette_radius = 1.0;
    extern number vignette_softness = 0.45;
    extern number vignette_opacity = 0.5;
    extern number aspect;

    extern number t = 0.0;

    number rand(vec2 co) {
      return Texel(noisetex, mod(co * tex_ratio / vec2(grain_size), vec2(1.0))).r;
    }

    vec4 effect(vec4 color, Image tex, vec2 uv, vec2 px) {
      // BARRELL DISTORTION
      // to barrel coordinates
      uv = uv * 2.0 - vec2(1.0);

      // distort
      uv /= .99;
      uv += (uv.yx*uv.yx) * uv * magnitude;
      number mask = (1.0 - smoothstep(.99,1.0,abs(uv.x))) * (1.0 - smoothstep(.99,1.0,abs(uv.y)));

      // to cartesian coordinates
      uv = (uv + vec2(1.0)) / 2.0;

      // "MELT" DISTORTION
      float d = pow(sin(200.*uv.y - 150.*uv.x + t*10.), 3.0) * melt;
      uv += d;

      // sample texture
      color *= Texel(tex, uv) * mask;

      // FILMGRAIN
      color *= mix(1.0, rand(uv+vec2(noise)), grain_opacity);

      // VIGNETTE
      number v = smoothstep(vignette_radius, vignette_radius-vignette_softness,
                            length((uv - vec2(0.5f)) * aspect));
      return mix(color, color * v, vignette_opacity);
    }
  ]]
  degrade:send("noisetex", noise)
  degrade:send("tex_ratio", {
    love.graphics.getWidth() / noise:getWidth(),
    love.graphics.getHeight() / noise:getHeight()})
  degrade:send("aspect", love.graphics.getWidth() / love.graphics.getHeight())

  -- Parameter setters
  local setters = {}

  setters.t = function(v) degrade:send("t", v) end
  setters.melt = function(v) degrade:send("melt", v) end

  -- godsray
  for _,k in ipairs{"exposure", "decay", "density", "weight"} do
    setters[k] = function(v)
      godsray:send(k, math.min(1, math.max(0, tonumber(v) or 0)))
    end
  end

  -- crt
  setters.magnitude = function(v) degrade:send("magnitude", v) end

  -- filmgrain
  for _,k in ipairs{"grain_opacity", "grain_size"} do
    setters[k] = function(v)
      degrade:send(k, math.max(0, tonumber(v) or 0))
    end
  end

  -- vignette
  for _,k in ipairs{"vignette_radidus", "vignette_softness", "vignette_opacity"} do
    setters[k] = function(v)
      degrade:send(k, math.max(0, tonumber(v) or 0))
    end
  end

  return setmetatable({
    draw = function(self, func, ...)
      -- scene
      love.graphics.setCanvas(buffer())
      love.graphics.clear()
      func(...)

      -- setup
      love.graphics.setColor(255,255,255,255)
      love.graphics.setBlendMode("alpha", "premultiplied")

      -- first pass: godsray
      love.graphics.setCanvas(buffer())
      love.graphics.clear()
      love.graphics.setShader(godsray)
      love.graphics.draw(back,0,0)

      -- second pass: crt
      love.graphics.setCanvas(buffer())
      love.graphics.clear()
      love.graphics.setShader(degrade)
      love.graphics.draw(back,0,0)

      -- finally: draw
      love.graphics.setShader()
      love.graphics.setCanvas()
      love.graphics.draw(front,0,0)

      love.graphics.setBlendMode("alpha")
    end,
  }, {
    __newindex = function(_, k, v)
      (setters[k] or print)(v) -- meh
    end
  })
end
