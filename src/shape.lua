local Shape = {}
Shape.__index = Shape

function Shape.new(d)
  return setmetatable(d, Shape)
end

function Shape:clone()
  local ret = {}
  for i,v in ipairs(self) do
    ret[i] = { {unpack(v[1])}, {unpack(v[2])} }
  end
  return Shape.new(ret)
end

function Shape:scale(sx,sy,sz)
  sx = sx or 1
  sy, sz = sy or sx, sz or sx
  for _,v in ipairs(self) do
    for i = 1,2 do
      v[i][1] = v[i][1] * sx
      v[i][2] = v[i][2] * sy
      v[i][3] = v[i][3] * sz
    end
  end
  return self
end

function Shape:translate(x, y, z)
  x, y, z = x or 0, y or 0, z or 0

  for n,v in ipairs(self) do
    for i = 1,2 do
      v[i][1] = v[i][1] + x
      v[i][2] = v[i][2] + y
      v[i][3] = v[i][3] + z
    end
  end
  return self
end

function Shape:add(other)
  assert(getmetatable(self) == getmetatable(other))
  for n,v in ipairs(self) do
    local w = other[n]
    for i = 1,2 do
      for k = 1,3 do
        v[i][k] = v[i][k] + w[i][k]
      end
    end
  end
  return self
end

function Shape:sub(other)
  assert(getmetatable(self) == getmetatable(other))
  for n,v in ipairs(self) do
    local w = other[n]
    for i = 1,2 do
      for k = 1,3 do
        v[i][k] = v[i][k] - w[i][k]
      end
    end
  end
  return self
end

function Shape.__add(this, other)
  return this:clone():add(other)
end

function Shape.__sub(this, other)
  return this:clone():sub(other)
end

function Shape.__mul(this, s)
  if type(this) == "number" then
    this, s = s, this
  end
  return this:clone():scale(s)
end

function Shape:draw(camera, jitter)
  jitter = jitter or 0
  for i,v in pairs(self) do
    local dx,dy,dz = 0,0,0
    if jitter > 0 then
      local theta, phi =  math.random()*2*math.pi, math.random()*2*math.pi
      dx = math.sin(theta)*math.cos(phi) * jitter * 4
      dy = math.sin(theta)*math.sin(phi) * jitter
      dz = math.cos(theta) * jitter
    end
    camera:line(v[1][1]+dx,v[1][2]+dy,v[1][3]+dz, v[2][1]+dz,v[2][2]+dx,v[2][3]+dy)
  end
end

function Shape:eyes(camera)
  local left, right, v = {}, {}

  v = self[67]
  left[#left+1], left[#left+2] = camera:transform(v[2][1],v[2][2],v[2][3])
  for k,i in ipairs{74,76,81,80,79,67} do
    v = self[i]
    left[#left+1], left[#left+2] = camera:transform(v[1][1],v[1][2],v[1][3])
  end

  v = self[65]
  right[#right+1], right[#right+2] = camera:transform(v[2][1],v[2][2],v[2][3])
  for k,i in ipairs{69,71,84,83,82,65} do
    v = self[i]
    right[#right+1], right[#right+2] = camera:transform(v[1][1],v[1][2],v[1][3])
  end

  return left, right
end

return Shape.new
